<?php

namespace DonnezOrg\SellsyClient\Tests\Endpoint;

use DonnezOrg\SellsyClient\Entity\Address\{AddressCreator, AddressMutator, GeoCode\GeoCode, GeoCode\GeoCodeCreator};
use DonnezOrg\SellsyClient\Entity\Company\{CompanyCreator, CompanyMutator};
use DonnezOrg\SellsyClient\Entity\CustomField\CustomFieldMutator;
use DonnezOrg\SellsyClient\Entity\Enum\{CompanyType, MarketingCampaignSubscription};
use DonnezOrg\SellsyClient\Entity\LegalFrance;
use DonnezOrg\SellsyClient\Entity\SmartTag\{SmartTagLinkByIdentifierCreator, SmartTagLinkByValueCreator};
use GuzzleHttp\Exception\GuzzleException;

use function PHPUnit\Framework\assertEquals;

final class CompanyTest extends EndpointTestCase
{
    /**
     * @throws GuzzleException
     */
    public function testCompaniesGet(): void
    {
        $this->addMockedResponse('companies/get_one.json');
        $company = $this->client->companies()->get(6657);
        $this->assertEquals('Example company', $company->getName());
        $this->assertEquals(CompanyType::PROSPECT, $company->getType());
        $this->assertContains(MarketingCampaignSubscription::SMS, $company->getMarketingCampaignsSubscriptions());
    }

    /**
     * @throws GuzzleException
     */
    public function testCompaniesGetAll(): void
    {
        $this->addMockedResponse('companies/get_all.json');
        $result = $this->client->companies()->getAll();
        $pagination = $result->getPagination();

        $this->assertEquals(1, $pagination->getCount());
        $this->assertEquals(25, $pagination->getLimit());
        $this->assertEquals(25, $pagination->getOffset());
        $this->assertEquals(152, $pagination->getTotal());
        $this->assertCount(1, $result->getData());

        $firstResult = $result->getData()[0];
        $this->assertEquals('Example company', $firstResult->getName());
        $this->assertEquals(CompanyType::PROSPECT, $firstResult->getType());
        $this->assertContains(MarketingCampaignSubscription::SMS, $firstResult->getMarketingCampaignsSubscriptions());
    }

    /**
     * @throws GuzzleException
     */
    public function testCompaniesCreate(): void
    {
        $this->addMockedResponse('companies/create.json');
        $company = $this->client->companies()->create(
            (new CompanyCreator(CompanyType::CLIENT, 'PHPUnit company'))
                ->addMarketingCampaignsSubscription(MarketingCampaignSubscription::EMAIL)
                ->addMarketingCampaignsSubscription(MarketingCampaignSubscription::PHONE)
        );
        $this->assertEquals('Example company', $company->getName());
        $this->assertEquals(CompanyType::PROSPECT, $company->getType());
        $this->assertContains(MarketingCampaignSubscription::SMS, $company->getMarketingCampaignsSubscriptions());
    }

    /**
     * @throws GuzzleException
     */
    public function testCompaniesUpdate(): void
    {
        $this->addMockedResponse('companies/update.json');
        $company = $this->client->companies()->update(
            39311461,
            (new CompanyMutator())
                ->setName('New name')
                ->setLegalFrance(
                    (new LegalFrance())
                        ->setSiret('53051415700027')
                )
        );

        $this->assertEquals('Example company', $company->getName());
        $this->assertEquals(CompanyType::PROSPECT, $company->getType());
        $this->assertContains(MarketingCampaignSubscription::SMS, $company->getMarketingCampaignsSubscriptions());
    }

    /**
     * @throws GuzzleException
     */
    public function testCompaniesAddressesGet(): void
    {
        $this->addMockedResponse('companies/addresses/get_one.json');
        $address = $this->client->companies()->getAddress(6657, 6657);
        $this->assertEquals('Main office', $address->getName());
        $this->assertTrue($address->getIsDeliveryAddress());
        $this->assertEquals(46.140862, $address->getGeocode()->getLat());
    }

    /**
     * @throws GuzzleException
     */
    public function testCompaniesAddressesGetAll(): void
    {
        $this->addMockedResponse('companies/addresses/get_all.json');
        $result = $this->client->companies()->getAddresses(6657);

        $this->assertCount(1, $result->getData());

        $firstResult = $result->getData()[0];
        $this->assertEquals('Main office', $firstResult->getName());
        $this->assertTrue($firstResult->getIsDeliveryAddress());
        $this->assertEquals(46.140862, $firstResult->getGeocode()->getLat());
    }

    /**
     * @throws GuzzleException
     */
    public function testCompaniesAddressesCreate(): void
    {
        $this->addMockedResponse('companies/addresses/create.json');
        $address = $this->client->companies()->createAddress(
            6657,
            (new AddressCreator('Main office', '30 Commercial Road', 'Paris', 'France', 'FR'))
                ->setGeocode(new GeoCodeCreator(46.140862, -1.168249))
        );
        $this->assertEquals('Main office', $address->getName());
        $this->assertEquals(46.140862, $address->getGeocode()->getLat());
    }

    /**
     * @throws GuzzleException
     */
    public function testCompaniesAddressesUpdate(): void
    {
        $this->addMockedResponse('companies/addresses/create.json');
        $address = $this->client->companies()->updateAddress(
            6657,
            2,
            (new AddressMutator())
                ->setGeocode(
                    (new GeoCode())
                        ->setLat(47.140862)
                )
        );
        // $this->assertEquals(47.140862, $address->getGeocode()->getLat());
        $this->assertTrue(true);
    }

    /**
     * @throws GuzzleException
     */
    public function testCompaniesContactLinkCreate(): void
    {
        $this->addMockedResponse(null, 201);
        $this->client->companies()->createContactLink(1, 1);
        $this->assertTrue(true);
    }

    /**
     * @throws GuzzleException
     */
    public function testCompaniesCustomFieldsUpdate(): void
    {
        $this->addMockedResponse();
        $this->client->companies()->updateCustomFields(
            6657,
            [new CustomFieldMutator(
                1,
                2
            )]
        );
        $this->assertTrue(true);
    }

    /**
     * @throws GuzzleException
     */
    public function testLinkSmartTags(): void
    {
        $this->addMockedResponse('companies/link_smart_tag.json');
        $response = $this->client->companies()->linkSmartTags(
            6657,
            new SmartTagLinkByIdentifierCreator(1),
            new SmartTagLinkByValueCreator('my-tag-2')
        );

        assertEquals(1, $response->getData()[0]->getId());
        assertEquals('my-tag-1', $response->getData()[0]->getValue());
    }
}
