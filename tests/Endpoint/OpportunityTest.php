<?php

namespace DonnezOrg\SellsyClient\Tests\Endpoint;

use DonnezOrg\SellsyClient\Entity\CustomField\CustomFieldMutator;
use DonnezOrg\SellsyClient\Entity\Enum\{CurrencyType, OpportunityStatus, RelationType};
use DonnezOrg\SellsyClient\Entity\Opportunity\OpportunityCreator;
use DonnezOrg\SellsyClient\Entity\{CustomField\AmountWithCurrencyCustomField,
    CustomField\Parameter\AmountWithCurrencyParameter,
    CustomField\Parameter\SimpleTextParameter,
    CustomField\SimpleTextCustomField,
    Opportunity\OpportunityMutator,
    Opportunity\SearchFilter,
    Relation
};
use GuzzleHttp\Exception\GuzzleException;

use function PHPUnit\Framework\assertEquals;

final class OpportunityTest extends EndpointTestCase
{
    /**
     * @throws GuzzleException
     */
    public function testOpportunitiesGet(): void
    {
        $this->addMockedResponse('opportunities/get_one.json');
        $opportunity = $this->client->opportunities()->get(1);
        $this->assertEquals('1970-01-01', $opportunity->getDueDate()?->format('Y-m-d'));
        $this->assertEquals(OpportunityStatus::WON, $opportunity->getStatus());
        $this->assertEquals(RelationType::COMPANY, $opportunity->getRelated()[0]->getType());
    }

    /**
     * @throws GuzzleException
     */
    public function testOpportunitiesGetAll(): void
    {
        $this->addMockedResponse('opportunities/get_all.json');
        $result = $this->client->opportunities()->getAll();
        $pagination = $result->getPagination();

        $this->assertEquals(25, $pagination->getCount());
        $this->assertEquals(25, $pagination->getLimit());
        $this->assertEquals(25, $pagination->getOffset());
        $this->assertEquals(152, $pagination->getTotal());
        $this->assertCount(1, $result->getData());

        $firstResult = $result->getData()[0];
        $this->assertEquals('1970-01-01', $firstResult->getDueDate()?->format('Y-m-d'));
        $this->assertEquals(OpportunityStatus::WON, $firstResult->getStatus());
    }

    /**
     * @throws GuzzleException
     */
    public function testOpportunitiesSearch(): void
    {
        $this->addMockedResponse('opportunities/search.json');
        $result = $this->client->opportunities()->search(new SearchFilter());
        $this->assertEquals('OPP-1155', $result->getData()[0]->getNumber());
    }

    /**
     * @throws GuzzleException
     */
    public function testOpportunityCreate(): void
    {
        $this->addMockedResponse('opportunities/create.json');
        $opportunity = $this->client->opportunities()->create(
            new OpportunityCreator('New website', 9763, 299, [new Relation(1, RelationType::COMPANY)])
        );

        $this->assertEquals('1970-01-01', $opportunity->getDueDate()?->format('Y-m-d'));
        $this->assertEquals(OpportunityStatus::WON, $opportunity->getStatus());
        $this->assertEquals(RelationType::COMPANY, $opportunity->getRelated()[0]->getType());
    }

    /**
     * @throws GuzzleException
     */
    public function testOpportunitiesUpdate(): void
    {
        $this->addMockedResponse('opportunities/update.json');
        $opportunity = $this->client->opportunities()->update(
            1,
            (new OpportunityMutator())
                ->setStep(299)
        );

        $this->assertTrue(true);
    }

    /**
     * @throws GuzzleException
     */
    public function testOpportunitiesCustomFieldsUpdate(): void
    {
        $this->addMockedResponse();
        $this->client->opportunities()->updateCustomFields(
            6657,
            [
                new CustomFieldMutator(
                    1,
                    2
                ),
            ]
        );
        $this->assertTrue(true);
    }

    /**
     * @throws GuzzleException
     */
    public function testOpportunitiesCustomFieldsGetList(): void
    {
        $this->addMockedResponse('opportunities/get_custom_fields.json');
        $result = $this->client->opportunities()->getCustomFields(6657);
        $simpleText = $result->getData()[0];
        $amountWithCurrency = $result->getData()[1];

        $this->assertInstanceOf(SimpleTextCustomField::class, $simpleText);
        $this->assertInstanceOf(SimpleTextParameter::class, $simpleText->getParameters());
        $this->assertEquals('Business sector', $simpleText->getName());
        $this->assertEquals('simple-text', $simpleText->getType());

        $this->assertInstanceOf(AmountWithCurrencyCustomField::class, $amountWithCurrency);
        $this->assertInstanceOf(AmountWithCurrencyParameter::class, $amountWithCurrency->getParameters());
        $this->assertEquals('10', $amountWithCurrency->getValue()->getAmount());
        $this->assertEquals(CurrencyType::EURO, $amountWithCurrency->getValue()->getCurrency());
        $this->assertEquals('amount', $amountWithCurrency->getType());
        $this->assertEquals(CurrencyType::EURO, $amountWithCurrency->getParameters()->getCurrency());
    }

    /**
     * @throws GuzzleException
     */
    public function testOpportunitiesCustomFieldsGetListUnhandled(): void
    {
        $this->logger->expects(self::once())->method('warning')->with('Custom field of type checkbox not handled for id 2 and name Choice');

        $this->addMockedResponse('opportunities/get_unhandled_custom_field.json');
        $result = $this->client->opportunities()->getCustomFields(6657);
        $data = $result->getData();
        $businessSector = $data[0];
        $unhandledCustomField = $data[1];
        $totalPrice = $data[2];

        assertEquals('Business sector', $businessSector->getName());

        assertEquals(2, $unhandledCustomField->getId());
        assertEquals('Choice', $unhandledCustomField->getName());
        assertEquals('checkbox', $unhandledCustomField->getType());
        assertEquals('The type of this custom field is not handled by the sellsy client', $unhandledCustomField->getDescription());

        assertEquals('Total price', $totalPrice->getName());
    }
}
