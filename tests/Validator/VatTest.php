<?php

namespace DonnezOrg\SellsyClient\Tests\Validator;

final class VatTest extends ValidatorTestCase
{
    public function testInvalidCountryCode(): void
    {
        $result = $this->validator->validate(new VatDto('AZ00999999999'));
        $this->assertCount(1, $result);
        $this->assertEquals('Invalid country code', $result[0]->getMessage());
    }

    public function testInvalidVatCode(): void
    {
        $result = $this->validator->validate(new VatDto('FR0099999999A'));
        $this->assertCount(1, $result);
        $this->assertEquals('Invalid VAT code', $result[0]->getMessage());
    }

    public function testNull(): void
    {
        $result = $this->validator->validate(new VatDto(null));
        $this->assertCount(0, $result);
    }

    public function testValid(): void
    {
        $result = $this->validator->validate(new VatDto('FR00999999999'));
        $this->assertCount(0, $result);
    }
}

class VatDto
{
    public function __construct(
        #[\DonnezOrg\SellsyClient\Validator\Vat]
        public ?string $vat
    ) {
    }
}
