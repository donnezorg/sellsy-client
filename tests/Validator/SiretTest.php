<?php

namespace DonnezOrg\SellsyClient\Tests\Validator;

use DonnezOrg\SellsyClient\Validator\Siret;

final class SiretTest extends ValidatorTestCase
{
    public function testInvalidFormat(): void
    {
        $result = $this->validator->validate(new SiretDto('530 514 157 00029'));
        $this->assertCount(1, $result);
        $this->assertEquals('Value should be a numeric string', $result[0]->getMessage());
    }

    public function testInvalidLength(): void
    {
        $result = $this->validator->validate(new SiretDto('530514157000290'));
        $this->assertCount(1, $result);
        $this->assertEquals('Value length should be equal to 14 characters', $result[0]->getMessage());
    }

    public function testNull(): void
    {
        $result = $this->validator->validate(new SiretDto(null));
        $this->assertCount(0, $result);
    }

    public function testValid(): void
    {
        $result = $this->validator->validate(new SiretDto('53051415700029'));
        $this->assertCount(0, $result);
    }
}

class SiretDto
{
    public function __construct(
        #[Siret]
        public ?string $siret
    ) {
    }
}
