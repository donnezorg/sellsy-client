<?php

namespace DonnezOrg\SellsyClient\Tests\Validator;

use Doctrine\Common\Annotations\AnnotationReader;
use DonnezOrg\SellsyClient\Core\SimpleTranslator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\Context\ExecutionContextFactory;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Validator\Validator\{RecursiveValidator, ValidatorInterface};

class ValidatorTestCase extends TestCase
{
    protected ValidatorInterface $validator;

    protected function setUp(): void
    {
        /**
         * @psalm-suppress InternalClass
         * @psalm-suppress InternalMethod
         */
        $contextFactory = new ExecutionContextFactory(new SimpleTranslator());
        $classMetadataFactory = new LazyLoadingMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $validatorFactory = new ConstraintValidatorFactory();

        $this->validator = new RecursiveValidator($contextFactory, $classMetadataFactory, $validatorFactory);
    }
}
