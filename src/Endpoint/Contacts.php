<?php

namespace DonnezOrg\SellsyClient\Endpoint;

use DonnezOrg\SellsyClient\Core\{Client, HttpMethod};
use DonnezOrg\SellsyClient\Entity\CollectionResult;
use DonnezOrg\SellsyClient\Entity\Contact\{Contact, ContactCreator};
use DonnezOrg\SellsyClient\Entity\CustomField\CustomFieldMutator;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Contacts extends AbstractEndpoint
{
    public function __construct(
        Client $client,
        HttpClient $httpClient,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        parent::__construct(
            'contacts',
            Contact::class,
            $client,
            $httpClient,
            $serializer,
            $validator
        );
    }

    /**
     * @throws GuzzleException
     */
    public function get(int $id): Contact
    {
        return $this->_get($id);
    }

    /**
     * Get a collection² of companies.
     *
     * @return CollectionResult<Contact>
     *
     * @throws GuzzleException
     */
    public function getAll(): CollectionResult
    {
        return $this->_getAll();
    }

    /**
     * @throws GuzzleException
     */
    public function create(ContactCreator $creator): Contact
    {
        return $this->_create($creator);
    }

    /**
     * @param int                  $contactId the contact id
     * @param CustomFieldMutator[] $mutator
     *
     * @throws GuzzleException
     */
    public function updateCustomFields(int $contactId, array $mutator): void
    {
        $this->request(HttpMethod::PUT, "$this->path/$contactId/custom-fields", ['data' => $mutator, 'responseType' => null]);
    }
}
