<?php

namespace DonnezOrg\SellsyClient\Endpoint;

use DonnezOrg\SellsyClient\Core\{Client, HttpMethod};
use DonnezOrg\SellsyClient\Entity\Company\{Company, CompanyMutator};
use DonnezOrg\SellsyClient\Entity\{CollectionResult, EntityCreator, EntityMutator};
use DonnezOrg\SellsyClient\Exception\ValidationException;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractEndpoint
{
    public function __construct(
        protected string $path,
        protected string $responseType,
        protected Client $client,
        protected HttpClient $httpClient,
        protected SerializerInterface $serializer,
        protected ValidatorInterface $validator
    ) {
    }

    /**
     * @throws GuzzleException
     */
    protected function _get(int $id): mixed
    {
        return $this->request(HttpMethod::GET, "$this->path/$id");
    }

    /**
     * Get a collection of companies.
     *
     * @throws GuzzleException
     */
    protected function _getAll(): CollectionResult
    {
        return $this->request(
            HttpMethod::GET,
            $this->path,
            ['responseType' => CollectionResult::class.'<'.$this->responseType.'>']
        );
    }

    /**
     * @throws GuzzleException
     */
    protected function _create(EntityCreator $creator): mixed
    {
        return $this->request(HttpMethod::POST, $this->path, ['data' => $creator]);
    }

    /**
     * @throws GuzzleException
     */
    protected function _update(int $id, EntityMutator $mutator): mixed
    {
        return $this->request(HttpMethod::PUT, "$this->path/$id", ['data' => $mutator]);
    }

    /**
     * @throws GuzzleException
     */
    public function delete(int $id): void
    {
        $this->request(HttpMethod::DELETE, "$this->path/$id");
    }

    /**
     * @param HttpMethod $method the http method of the request
     * @param string     $path   the path to make the request on
     *
     * @throws GuzzleException
     * @throws \Exception      if the http method is not handled
     */
    protected function request(HttpMethod $method, string $path, array $options = []): mixed
    {
        $response = null;
        $responseType = $options['responseType'] ?? $this->responseType;

        switch ($method) {
            case HttpMethod::GET:
                $response = $this->httpClient->get($path);
                break;
            case HttpMethod::POST:
            case HttpMethod::PUT:
            case HttpMethod::PATCH:
                $data = $options['data'];
                $body = '';
                if (isset($data)) {
                    $result = $this->validator->validate($data);
                    if (count($result) > 0) {
                        throw new ValidationException($result);
                    }
                    $body = $this->serializer->serialize($data, 'json');
                }

                $response = $this->httpClient->{strtolower($method->name)}($path, [
                    'body' => $body,
                ]);
                break;
            case HttpMethod::DELETE:
                $this->httpClient->delete($path);
                break;
            default:
                throw new \Exception("Method $method->name not handled");
        }

        if (null !== $response && 0 !== $response->getBody()->getSize()) {
            return $this->serializer->deserialize(
                $response->getBody()->getContents(),
                $responseType,
                'json'
            );
        } else {
            return null;
        }
    }
}
