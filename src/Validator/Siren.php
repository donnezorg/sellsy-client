<?php

/** @noinspection PhpMultipleClassDeclarationsInspection */

namespace DonnezOrg\SellsyClient\Validator;

use Attribute;
use Symfony\Component\Validator\Constraint;

#[\Attribute]
class Siren extends Constraint
{
}
