<?php

namespace DonnezOrg\SellsyClient\Validator;

use Symfony\Component\Validator\{Constraint, ConstraintValidator};

class SirenValidator extends ConstraintValidator
{
    protected int $length = 9;

    /**
     * {@inheritDoc}
     */
    public function validate(mixed $value, Constraint $constraint)
    {
        if (null !== $value) {
            if (!is_string($value) || !is_numeric($value)) {
                $this->context->addViolation('Value should be a numeric string');
            } elseif (strlen($value) != $this->length) {
                $this->context
                    ->buildViolation('Value length should be equal to {{ length }} characters')
                    ->setParameter('{{ length }}', (string) $this->length)
                    ->addViolation();
            } else {
                $sum = 0;
                for ($i = 0; $i < $this->length; ++$i) {
                    $index = ($this->length - $i);
                    $tmp = (2 - ($index % 2)) * intval($value[$i]);
                    if ($tmp >= 10) {
                        $tmp -= 9;
                    }
                    $sum += $tmp;
                }

                if (($sum % 10) !== 0) {
                    $this->context->addViolation('Value is invalid');
                }
            }
        }
    }
}
