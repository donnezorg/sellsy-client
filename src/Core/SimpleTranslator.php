<?php

namespace DonnezOrg\SellsyClient\Core;

use Symfony\Contracts\Translation\TranslatorInterface;

class SimpleTranslator implements TranslatorInterface
{
    public function trans(string $id, array $parameters = [], string $domain = null, string $locale = null): string
    {
        foreach ($parameters as $key => $value) {
            $id = str_replace($key, $value, $id);
        }

        return $id;
    }

    public function getLocale(): string
    {
        return 'en_US';
    }
}
