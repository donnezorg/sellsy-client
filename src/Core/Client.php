<?php

namespace DonnezOrg\SellsyClient\Core;

use Doctrine\Common\Annotations\AnnotationReader;
use DonnezOrg\SellsyClient\Endpoint\{Companies, Contacts, Opportunities};
use DonnezOrg\SellsyClient\Entity\Token;
use DonnezOrg\SellsyClient\Helpers\CamelCaseToSnakeCaseNameConverter;
use DonnezOrg\SellsyClient\Serializer\{CollectionResultNormalizer, CustomFieldNormalizer};
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\{BodySummarizer, Client as HttpClient, HandlerStack, MessageFormatter, Middleware};
use Psr\Http\Message\RequestInterface;
use Psr\Log\{LoggerInterface, NullLogger};
use Symfony\Component\PropertyInfo\Extractor\{PhpDocExtractor, ReflectionExtractor};
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\{Mapping\Factory\ClassMetadataFactory,
    Mapping\Loader\AnnotationLoader,
    Normalizer\ArrayDenormalizer,
    Normalizer\BackedEnumNormalizer,
    Normalizer\DateTimeNormalizer,
    Serializer,
    SerializerInterface
};
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\Context\ExecutionContextFactory;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Validator\{RecursiveValidator, ValidatorInterface};

class Client
{
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;

    private Companies $companies;
    private Contacts $contacts;
    private Opportunities $opportunities;
    private ?Token $token = null;

    public function __construct(
        private readonly string $clientId,
        private readonly string $clientSecret,
        private ?LoggerInterface $logger = null,
        private ?HttpClient $httpClient = null
    ) {
        if (null === $this->logger) {
            $this->logger = new NullLogger();
        }

        if (null === $this->httpClient) {
            // Create http client
            $stack = new HandlerStack();
            $stack->setHandler(new CurlHandler());
            $stack->push(
                Middleware::mapRequest(function (RequestInterface $request) {
                    // Avoid infinite loop
                    if ('login.sellsy.com' !== $request->getUri()->getHost()) {
                        return $request->withHeader('Authorization', 'Bearer '.$this->getToken()->getAccessToken());
                    }

                    return $request;
                })
            );
            $stack->push(Middleware::httpErrors(new BodySummarizer(1500)), 'http_errors');
            $stack->push(
                Middleware::log(
                    $this->logger,
                    new MessageFormatter('"{method} {uri} HTTP/{version}" {code} {req_body}')
                )
            );

            $this->httpClient = new HttpClient([
                'handler' => $stack,
                'timeout' => 10,
                'base_uri' => 'https://api.sellsy.com/v2/',
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
            ]);
        }

        // Create serializer
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $extractors = new PropertyInfoExtractor(
            [],
            [new PhpDocExtractor(), new ReflectionExtractor()]
        );
        $this->serializer = new Serializer(
            [
                new ArrayDenormalizer(),
                new CollectionResultNormalizer(),
                new CustomFieldNormalizer($this->logger),
                new BackedEnumNormalizer(),
                new DateTimeNormalizer(),
                new ObjectNormalizer(
                    $classMetadataFactory,
                    new CamelCaseToSnakeCaseNameConverter(),
                    null,
                    $extractors
                ),
            ],
            [new JsonEncoder()]
        );

        // Create validator
        /**
         * @psalm-suppress InternalClass
         * @psalm-suppress InternalMethod
         */
        $contextFactory = new ExecutionContextFactory(new SimpleTranslator());
        $classMetadataFactory = new LazyLoadingMetadataFactory(
            new \Symfony\Component\Validator\Mapping\Loader\AnnotationLoader(new AnnotationReader())
        );
        $validatorFactory = new ConstraintValidatorFactory();
        $this->validator = new RecursiveValidator($contextFactory, $classMetadataFactory, $validatorFactory);

        // Create endpoints
        $this->companies = new Companies($this, $this->httpClient, $this->serializer, $this->validator);
        $this->contacts = new Contacts($this, $this->httpClient, $this->serializer, $this->validator);
        $this->opportunities = new Opportunities($this, $this->httpClient, $this->serializer, $this->validator);
    }

    /**
     * Get endpoints for companies.
     */
    public function companies(): Companies
    {
        return $this->companies;
    }

    /**
     * Get endpoints for companies.
     */
    public function contacts(): Contacts
    {
        return $this->contacts;
    }

    /**
     * Get endpoints for opportunities.
     */
    public function opportunities(): Opportunities
    {
        return $this->opportunities;
    }

    /**
     * Get an OAuth access token.
     *
     * @throws GuzzleException
     */
    private function getToken(): Token
    {
        if (null === $this->token) {
            /** @psalm-suppress PossiblyNullReference */
            $response = $this->httpClient->post('https://login.sellsy.com/oauth2/access-tokens', [
                'body' => json_encode([
                    'grant_type' => 'client_credentials',
                    'client_id' => $this->clientId,
                    'client_secret' => $this->clientSecret,
                ]),
            ]);

            $this->token = $this->serializer->deserialize($response->getBody()->getContents(), Token::class, 'json');
        }

        return $this->token;
    }

    /** @psalm-suppress NullableReturnStatement, InvalidNullableReturnType */
    public function getHttpClient(): HttpClient
    {
        return $this->httpClient;
    }

    /** @psalm-suppress NullableReturnStatement, InvalidNullableReturnType */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }
}
