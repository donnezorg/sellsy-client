<?php

namespace DonnezOrg\SellsyClient\Serializer;

use DonnezOrg\SellsyClient\Entity\{CollectionResult, Pagination};
use Symfony\Component\Serializer\Normalizer\{DenormalizerAwareInterface, DenormalizerInterface};

class CollectionResultNormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    private DenormalizerInterface $denormalizer;

    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): CollectionResult
    {
        $matches = null;
        preg_match('/.*<(.*)>/', $type, $matches);
        $extractedType = $matches[1];

        $collection = new CollectionResult();
        $collection->setData($this->denormalizer->denormalize($data['data'], $extractedType.'[]', $format, $context));
        $collection->setPagination(
            $this->denormalizer->denormalize($data['pagination'], Pagination::class, $format, $context)
        );

        return $collection;
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool
    {
        return 1 === preg_match('/.*<.*>/', $type);
    }

    public function setDenormalizer(DenormalizerInterface $denormalizer)
    {
        $this->denormalizer = $denormalizer;
    }
}
