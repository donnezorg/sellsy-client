<?php

namespace DonnezOrg\SellsyClient\Entity;

use DonnezOrg\SellsyClient\Entity\Enum\RelationType;

final class Relation
{
    public function __construct(
        private int $id,
        private RelationType $type
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Relation
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): RelationType
    {
        return $this->type;
    }

    public function setType(RelationType $type): Relation
    {
        $this->type = $type;

        return $this;
    }
}
