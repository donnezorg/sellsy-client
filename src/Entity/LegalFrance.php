<?php

namespace DonnezOrg\SellsyClient\Entity;

use DonnezOrg\SellsyClient\Validator\{Siren};
use DonnezOrg\SellsyClient\Validator\{Siret, Vat};

final class LegalFrance
{
    #[Siret]
    private ?string $siret;

    #[Siren]
    private ?string $siren;

    #[Vat]
    private ?string $vat;

    private ?string $apeNafCode;
    private ?string $companyType;
    private ?string $rcsImmatriculation;

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren(?string $siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    public function getVat(): ?string
    {
        return $this->vat;
    }

    public function setVat(?string $vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    public function getApeNafCode(): ?string
    {
        return $this->apeNafCode;
    }

    public function setApeNafCode(?string $apeNafCode): self
    {
        $this->apeNafCode = $apeNafCode;

        return $this;
    }

    public function getCompanyType(): ?string
    {
        return $this->companyType;
    }

    public function setCompanyType(?string $companyType): self
    {
        $this->companyType = $companyType;

        return $this;
    }

    public function getRcsImmatriculation(): ?string
    {
        return $this->rcsImmatriculation;
    }

    public function setRcsImmatriculation(?string $rcsImmatriculation): self
    {
        $this->rcsImmatriculation = $rcsImmatriculation;

        return $this;
    }
}
