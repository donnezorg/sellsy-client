<?php

namespace DonnezOrg\SellsyClient\Entity;

final class Owner
{
    private int $id;
    private string $type;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Owner
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): Owner
    {
        $this->type = $type;

        return $this;
    }
}
