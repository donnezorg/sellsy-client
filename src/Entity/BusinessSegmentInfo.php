<?php

namespace DonnezOrg\SellsyClient\Entity;

class BusinessSegmentInfo
{
    private int $id;
    private string $label;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): BusinessSegmentInfo
    {
        $this->id = $id;

        return $this;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): BusinessSegmentInfo
    {
        $this->label = $label;

        return $this;
    }
}
