<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractCustomField
{
    protected int $id;

    protected mixed $value;

    #[Assert\Length(max: 250)]
    protected string $name;

    #[Assert\Length(max: 250)]
    protected string $code;

    #[Assert\Length(max: 250)]
    protected string $description;

    protected bool $mandatory;
    protected int $rank;

    /**
     * @var array<string>
     */
    protected array $relatedObject;

    protected CustomFieldGroup $customfieldGroup;

    protected mixed $parameters;

    abstract public function getType(): string;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isMandatory(): bool
    {
        return $this->mandatory;
    }

    public function setMandatory(bool $mandatory): self
    {
        $this->mandatory = $mandatory;

        return $this;
    }

    public function getRank(): int
    {
        return $this->rank;
    }

    public function setRank(int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getRelatedObject(): array
    {
        return $this->relatedObject;
    }

    public function setRelatedObject(array $relatedObject): self
    {
        $this->relatedObject = $relatedObject;

        return $this;
    }

    public function getCustomfieldGroup(): CustomFieldGroup
    {
        return $this->customfieldGroup;
    }

    public function setCustomfieldGroup(CustomFieldGroup $customfieldGroup): self
    {
        $this->customfieldGroup = $customfieldGroup;

        return $this;
    }
}
