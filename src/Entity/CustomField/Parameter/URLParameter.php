<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField\Parameter;

use Symfony\Component\Validator\Constraints as Assert;

class URLParameter
{
    #[Assert\Url]
    private ?string $defaultValue;

    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(?string $defaultValue): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }
}
