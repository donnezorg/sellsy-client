<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use DonnezOrg\SellsyClient\Entity\EntityMutator;

class CustomFieldMutator implements EntityMutator
{
    public function __construct(private int $id, private int|bool|string|array|null $value)
    {
    }

    /**
     * Get the value of id.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id.
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of value.
     */
    public function getValue(): int|bool|string|array|null
    {
        return $this->value;
    }

    /**
     * Set the value of value.
     */
    public function setValue(int|bool|string|array|null $value): self
    {
        $this->value = $value;

        return $this;
    }
}
