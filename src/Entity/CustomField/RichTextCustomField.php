<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use DonnezOrg\SellsyClient\Entity\CustomField\Parameter\RichTextParameter;

class RichTextCustomField extends AbstractCustomField
{
    public function getType(): string
    {
        return 'rich-text';
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    public function getParameters(): RichTextParameter
    {
        return $this->parameters;
    }

    public function setParameters(RichTextParameter $parameters): void
    {
        $this->parameters = $parameters;
    }
}
