<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use DonnezOrg\SellsyClient\Entity\CustomField\Parameter\NumericParameter;

class NumericCustomField extends AbstractCustomField
{
    public function getType(): string
    {
        return 'numeric';
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    public function getParameters(): NumericParameter
    {
        return $this->parameters;
    }

    public function setParameters(NumericParameter $parameters): void
    {
        $this->parameters = $parameters;
    }
}
