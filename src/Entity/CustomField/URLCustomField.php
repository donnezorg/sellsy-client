<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use DonnezOrg\SellsyClient\Entity\CustomField\Parameter\URLParameter;
use Symfony\Component\Validator\Constraints as Assert;

class URLCustomField extends AbstractCustomField
{
    public function getType(): string
    {
        return 'url';
    }

    #[Assert\Url]
    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    public function getParameters(): URLParameter
    {
        return $this->parameters;
    }

    public function setParameters(URLParameter $parameters): void
    {
        $this->parameters = $parameters;
    }
}
