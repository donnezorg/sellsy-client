<?php

namespace DonnezOrg\SellsyClient\Entity\CustomField;

use DonnezOrg\SellsyClient\Entity\CustomField\Parameter\AmountWithCurrencyParameter;
use DonnezOrg\SellsyClient\Entity\Enum\CurrencyType;
use Symfony\Component\Validator\Constraints\{Regex};

class AmountWithCurrencyCustomField extends AbstractCustomField
{
    public function getType(): string
    {
        return 'amount';
    }

    public function getValue(): AmountWithCurrency
    {
        return $this->value;
    }

    public function setValue(AmountWithCurrency $value): void
    {
        $this->value = $value;
    }

    public function getParameters(): AmountWithCurrencyParameter
    {
        return $this->parameters;
    }

    public function setParameters(AmountWithCurrencyParameter $parameters): void
    {
        $this->parameters = $parameters;
    }
}

class AmountWithCurrency
{
    private ?string $amount;
    private ?CurrencyType $currency;

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCurrency(): ?CurrencyType
    {
        return $this->currency;
    }

    public function setCurrency(?CurrencyType $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
