<?php

namespace DonnezOrg\SellsyClient\Entity;

class Range
{
    private float $min;
    private float $max;

    public function getMin(): float
    {
        return $this->min;
    }

    public function setMin(float $min): Range
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): float
    {
        return $this->max;
    }

    public function setMax(float $max): Range
    {
        $this->max = $max;

        return $this;
    }
}
