<?php

namespace DonnezOrg\SellsyClient\Entity;

use Symfony\Component\Validator\Constraints\DateTime;

class DateFilter
{
    private DateTime $start;
    private DateTime $end;

    public function getStart(): DateTime
    {
        return $this->start;
    }

    public function setStart(DateTime $start): DateFilter
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): DateTime
    {
        return $this->end;
    }

    public function setEnd(DateTime $end): DateFilter
    {
        $this->end = $end;

        return $this;
    }
}
