<?php

namespace DonnezOrg\SellsyClient\Entity\Company;

use DonnezOrg\SellsyClient\Entity\Enum\{MarketingCampaignSubscription};
use DonnezOrg\SellsyClient\Entity\{EntityMutator, LegalFrance, Social};

class CompanyMutator extends AbstractCompany implements EntityMutator
{
    private ?int $numberOfEmployees;
    private ?int $businessSegment;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getMobileNumber(): ?string
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber(?string $mobileNumber): self
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    public function getLegalFrance(): ?LegalFrance
    {
        return $this->legalFrance;
    }

    public function setLegalFrance(?LegalFrance $legalFrance): self
    {
        $this->legalFrance = $legalFrance;

        return $this;
    }

    public function getCapital(): ?string
    {
        return $this->capital;
    }

    public function setCapital(?string $capital): self
    {
        $this->capital = $capital;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getAuxiliaryCode(): ?string
    {
        return $this->auxiliaryCode;
    }

    public function setAuxiliaryCode(?string $auxiliaryCode): self
    {
        $this->auxiliaryCode = $auxiliaryCode;

        return $this;
    }

    public function getSocial(): ?Social
    {
        return $this->social;
    }

    public function setSocial(?Social $social): self
    {
        $this->social = $social;

        return $this;
    }

    public function getRateCategoryId(): ?int
    {
        return $this->rateCategoryId;
    }

    public function setRateCategoryId(?int $rateCategoryId): self
    {
        $this->rateCategoryId = $rateCategoryId;

        return $this;
    }

    public function getAccountingCodeId(): ?int
    {
        return $this->accountingCodeId;
    }

    public function setAccountingCodeId(?int $accountingCodeId): self
    {
        $this->accountingCodeId = $accountingCodeId;

        return $this;
    }

    public function getAccountingPurchaseCodeId(): ?int
    {
        return $this->accountingPurchaseCodeId;
    }

    public function setAccountingPurchaseCodeId(?int $accountingPurchaseCodeId): self
    {
        $this->accountingPurchaseCodeId = $accountingPurchaseCodeId;

        return $this;
    }

    public function getIsArchived(): bool
    {
        return $this->isArchived;
    }

    public function setIsArchived(bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    public function getNumberOfEmployees(): ?int
    {
        return $this->numberOfEmployees;
    }

    public function setNumberOfEmployees(?int $numberOfEmployees): self
    {
        $this->numberOfEmployees = $numberOfEmployees;

        return $this;
    }

    public function getBusinessSegment(): ?int
    {
        return $this->businessSegment;
    }

    public function setBusinessSegment(?int $businessSegment): self
    {
        $this->businessSegment = $businessSegment;

        return $this;
    }

    /**
     * @return array<MarketingCampaignSubscription>
     */
    public function getMarketingCampaignsSubscriptions(): array
    {
        return $this->marketingCampaignsSubscriptions;
    }

    /**
     * @param array<MarketingCampaignSubscription> $marketingCampaignsSubscriptions
     */
    public function setMarketingCampaignsSubscriptions(array $marketingCampaignsSubscriptions): self
    {
        $this->marketingCampaignsSubscriptions = $marketingCampaignsSubscriptions;

        return $this;
    }

    public function addMarketingCampaignsSubscription(MarketingCampaignSubscription $subscription): self
    {
        $this->marketingCampaignsSubscriptions[] = $subscription;

        return $this;
    }

    public function removeMarketingCampaignsSubscription(MarketingCampaignSubscription $subscription): self
    {
        $index = array_search($subscription, $this->marketingCampaignsSubscriptions);
        if (false !== $index) {
            array_splice($this->marketingCampaignsSubscriptions, $index, 1);
        }

        return $this;
    }
}
