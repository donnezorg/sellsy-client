<?php

namespace DonnezOrg\SellsyClient\Entity\Contact;

use DonnezOrg\SellsyClient\Entity\EntityCreator;

class ContactLinkCreator extends ContactLink implements EntityCreator
{
    public function setRoles(?array $roles): ContactLinkCreator
    {
        $this->roles = $roles;

        return $this;
    }
}
