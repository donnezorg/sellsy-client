<?php

namespace DonnezOrg\SellsyClient\Entity;

use Symfony\Component\Validator\Constraints as Assert;

final class Social
{
    #[Assert\Url]
    private ?string $twitter;

    #[Assert\Url]
    private ?string $facebook;

    #[Assert\Url]
    private ?string $linkedin;

    #[Assert\Url]
    private ?string $viadeo;

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): Social
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): Social
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getLinkedin(): ?string
    {
        return $this->linkedin;
    }

    public function setLinkedin(?string $linkedin): Social
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    public function getViadeo(): ?string
    {
        return $this->viadeo;
    }

    public function setViadeo(?string $viadeo): Social
    {
        $this->viadeo = $viadeo;

        return $this;
    }
}
