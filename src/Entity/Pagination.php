<?php

namespace DonnezOrg\SellsyClient\Entity;

class Pagination
{
    private int $limit;
    private int $count;
    private int $total;
    private int|string $offset;

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): Pagination
    {
        $this->limit = $limit;

        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): Pagination
    {
        $this->count = $count;

        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): Pagination
    {
        $this->total = $total;

        return $this;
    }

    public function getOffset(): int|string
    {
        return $this->offset;
    }

    public function setOffset(int|string $offset): Pagination
    {
        $this->offset = $offset;

        return $this;
    }
}
