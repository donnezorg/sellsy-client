<?php

namespace DonnezOrg\SellsyClient\Entity\Enum;

enum UnitType: string
{
    case UNIT = 'unit';
    case PACKAGE = 'package';
    case METER_SQUARE = 'm2';
    case METER_CUBE = 'm3';
    case TON = 'ton';
    case KG = 'kg';
    case G = 'g';
    case METER = 'metres';
    case DAY = 'days';
    case HOUR = 'hour';
    case MINUTE = 'minutes';
}
