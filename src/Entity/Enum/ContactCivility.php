<?php

namespace DonnezOrg\SellsyClient\Entity\Enum;

enum ContactCivility: string
{
    case MR = 'mr';
    case MRS = 'mrs';
    case MS = 'ms';
}
