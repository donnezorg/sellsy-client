<?php

namespace DonnezOrg\SellsyClient\Entity\Enum;

enum RelationType: string
{
    case COMPANY = 'company';
    case INDIVIDUAL = 'individual';
    case CONTACT = 'contact';
}
