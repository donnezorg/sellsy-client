<?php

namespace DonnezOrg\SellsyClient\Entity\Enum;

enum MarketingCampaignSubscription: string
{
    case SMS = 'sms';
    case PHONE = 'phone';
    case EMAIL = 'email';
    case POSTAL_MAIL = 'postal_mail';
    case CUSTOM = 'custom';
}
