<?php

namespace DonnezOrg\SellsyClient\Entity\Enum;

enum OpportunityStatus: string
{
    case OPEN = 'open';
    case WON = 'won';
    case LOST = 'lost';
    case CANCELLED = 'cancelled';
    case CLOSED = 'closed';
    case LATE = 'late';
}
