<?php

namespace DonnezOrg\SellsyClient\Entity\Address\GeoCode;

class GeoCode extends AbstractGeoCode
{
    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(?float $lat): GeoCode
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?float
    {
        return $this->lng;
    }

    public function setLng(?float $lng): GeoCode
    {
        $this->lng = $lng;

        return $this;
    }
}
