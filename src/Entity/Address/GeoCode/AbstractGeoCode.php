<?php

namespace DonnezOrg\SellsyClient\Entity\Address\GeoCode;

abstract class AbstractGeoCode
{
    protected ?float $lat;
    protected ?float $lng;
}
