<?php

namespace DonnezOrg\SellsyClient\Entity\Address;

use DonnezOrg\SellsyClient\Entity\Address\GeoCode\GeoCode;

class Address extends AbstractAddress
{
    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Address
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Address
    {
        $this->name = $name;

        return $this;
    }

    public function getAddressLine1(): ?string
    {
        return $this->addressLine1;
    }

    public function setAddressLine1(?string $addressLine1): Address
    {
        $this->addressLine1 = $addressLine1;

        return $this;
    }

    public function getAddressLine2(): ?string
    {
        return $this->addressLine2;
    }

    public function setAddressLine2(?string $addressLine2): Address
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    public function getAddressLine3(): ?string
    {
        return $this->addressLine3;
    }

    public function setAddressLine3(?string $addressLine3): Address
    {
        $this->addressLine3 = $addressLine3;

        return $this;
    }

    public function getAddressLine4(): ?string
    {
        return $this->addressLine4;
    }

    public function setAddressLine4(?string $addressLine4): Address
    {
        $this->addressLine4 = $addressLine4;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): Address
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): Address
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): Address
    {
        $this->country = $country;

        return $this;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function setCountryCode(string $countryCode): Address
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    public function getIsInvoicingAddress(): bool
    {
        return $this->isInvoicingAddress;
    }

    public function setIsInvoicingAddress(bool $isInvoicingAddress): Address
    {
        $this->isInvoicingAddress = $isInvoicingAddress;

        return $this;
    }

    public function getIsDeliveryAddress(): bool
    {
        return $this->isDeliveryAddress;
    }

    public function setIsDeliveryAddress(bool $isDeliveryAddress): Address
    {
        $this->isDeliveryAddress = $isDeliveryAddress;

        return $this;
    }

    public function getGeocode(): GeoCode
    {
        return $this->geocode;
    }

    public function setGeocode(GeoCode $geocode): Address
    {
        $this->geocode = $geocode;

        return $this;
    }
}
