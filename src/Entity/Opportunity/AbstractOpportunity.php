<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

use DonnezOrg\SellsyClient\Entity\Enum\OpportunityStatus;
use DonnezOrg\SellsyClient\Entity\Relation;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractOpportunity
{
    protected int $id;
    protected string $number;
    protected string $name;

    #[Context(
        normalizationContext: [DateTimeNormalizer::FORMAT_KEY => 'Y-m-d'],
        denormalizationContext: [DateTimeNormalizer::FORMAT_KEY => 'Y-m-d']
    )]
    protected ?\DateTime $dueDate;

    protected \DateTime $created;
    protected ?\DateTime $updatedStatus;
    protected OpportunityStatus $status;

    #[Assert\Range(min: 0, max: 100)]
    protected int $probability;

    protected string $note;

    /**
     * @var ?array<int>
     */
    protected ?array $assignedStaffIds;

    /**
     * @var ?array<int>
     */
    protected ?array $contactIds;

    protected ?int $companyId;
    protected ?int $individualId;
    protected ?int $mainDocId;

    /**
     * @var array<Relation>
     */
    #[Assert\Count(min: 1)]
    protected array $related;
}
