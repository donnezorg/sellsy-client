<?php

namespace DonnezOrg\SellsyClient\Entity\Opportunity;

final class Step
{
    private int $id;
    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Step
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Step
    {
        $this->name = $name;

        return $this;
    }
}
